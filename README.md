# Tezos-aws-private-node
## Prerequisite
- AWS account & CLI Tool
- Pulumi account & CLI Tool
- NodeJs
- [kubectl](https://kubernetes.io/docs/tasks/tools/)
- [k9s](https://github.com/derailed/k9s)

## AWS account & CLI Tool
 Create an AWS account if you don’t already have one, and install Pulumi. Your AWS IAM user must have the ability to create a [VPC](https://aws.amazon.com/fr/vpc/) and [EKS](https://aws.amazon.com/fr/eks/) cluster :
 - https://www.pulumi.com/docs/get-started/aws/begin/
  



## Pulumi 

Create a new [pulumi project](https://www.pulumi.com/docs/get-started/aws/create-project/)

Install dependencies 

```
npm i 
```
Install the [Tezos Pulumi](https://github.com/oxheadalpha/tezos-pulumi) node module which provides functionnality  to create both AWS and Kubernetes sources for your Tezos deployment.
```
npm install @oxheadalpha/tezos-pulumi --save-exact
```

-  ```values.yaml ```: file that specifies what we'd like the [tezos-k8s](https://github.com/oxheadalpha/tezos-k8s) Helm chart to deploy inside k8s.
-  ```index.ts```: a very customizable Typescript file which will deploy the Tezos infrastructure. 

## Deploy
To start the deployment 

```
pulumi up
```
You can use 
```pulumi preview```  to see the details of the infrastructures that will be installed or to see the updates before a new deployment. A graph is produced to resume their details:
- ``` AWS objects``` deployed by [AWS Provider](https://github.com/pulumi/pulumi-aws)
- ``` K8s obeccts``` deployed by [k8s Provider](https://github.com/pulumi/pulumi-kubernetes)

## Connecting to your Cluster with k9s

- Install k9s: 
  ```
  https://github.com/derailed/k9s
  ```
- Get the cluster's kubeconfig and save it to a file: 
```
pulumi stack output kubeconfig --show-secrets --json > kubeconfig.json
```
- Set the KUBECONFIG environment variable to point k9s to our kubeconfig.
```
export KUBECONFIG=./kubeconfig.json
```
## Let's get into our cluster!
```
k9s
```

Please note that you must unmount the cluster if you are only using this document for testing purposes to limit the cost of using AWS resources.

## Relative Links:
- ```OxheadAlpha Medium resource``` : https://medium.com/the-aleph/deploy-scalable-tezos-nodes-in-the-cloud-bbe4f4f4ddcc